# SpringBoot + Mysql + Docker

This is the complete Application to understand the Docker-Compose with **Spring-boot, Mysql, Docker and Docker-Compose**
### To Run Application using terminal
Make sure that post **8080** and **3306** is free in your syatem otherwise you change them form docker-compose as you need.
```sh
git clone https://gitlab.com/pushonly14u/springboot_mysql_dockercompose
cd springboot_mysql_dockercompose
mvn clean package
docker-compose build
docker-compose up
```

### Now Application is running on
http://localhost:8080/
http://localhost:8080/users

##To Strp it
**ctrl+c** 
and
```sh
docker-compose down
```
