package com.spring.controller;

import java.util.List;
import java.util.Optional;

import com.spring.model.User;
import com.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String home() {
		return "Application is Running";
	}

	@GetMapping("/users")
	public List<User> getAllTopics() {
		return userService.getAllUsers();
	}
	
	@GetMapping("users/{id}")
	public Optional<User> getTopic(@PathVariable String id) {
		return userService.getUser(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/users")
	public ResponseEntity<String> addTopic(@RequestBody User user){
		return userService.addUser(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/users/{id}", produces = {"application/json"})
	public HttpEntity<String> updateTopic(@RequestBody User user, @PathVariable String id){
		return userService.updateUser(id, user);

	}

	@RequestMapping(method=RequestMethod.DELETE, value="/users/{id}")
	public ResponseEntity<String> deleteTopic(@PathVariable String id){
		return userService.deleteUser(id);
		
	}
}