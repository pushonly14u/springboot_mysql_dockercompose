package com.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.spring.model.User;
import com.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	public List<User> getAllUsers(){
		List<User> users = new ArrayList<>();
		userRepository.findAll()
		.forEach(users::add);
		return users;
	}
	
	public Optional<User> getUser(String id) {
		return userRepository.findById(id);
	}

	public ResponseEntity<String> addUser(User user) {
		userRepository.save(user);
		return new ResponseEntity<>("Created", HttpStatus.CREATED);
		
	}
	
	public ResponseEntity<String> updateUser(String id, User user) {
		if(userRepository.existsById(id)){
			userRepository.save(user);
			return new ResponseEntity<>("Updated",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("Id not Registered",HttpStatus.OK);
		}
	}

	public ResponseEntity<String> deleteUser(String id) {
		if(userRepository.existsById(id)){
			userRepository.deleteById(id);
			return new ResponseEntity<>("Deleted",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("Id not Registered",HttpStatus.OK);
		}

	}
}
