package com.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseApiDataApplication {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Application waiting for DB initialization");
		Thread.sleep(1000*10);//TO wait for DB initialization
		SpringApplication.run(CourseApiDataApplication.class, args);
	}

}
